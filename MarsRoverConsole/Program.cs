﻿using MarsRover;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarsRoverConsole
{
    class Program
    {
        static IEngineParser parser;
        static IEngine engine;
        static Robot robot;
        static void Main(string[] args)
        {
            parser = new EngineParser();
            engine = new Engine(20, 20, parser);

            robot = new Robot(engine);
            Console.Clear();

            ConsoleKeyInfo key;
            Repaint();

            do
            {
                key = Console.ReadKey();

                if (key.KeyChar != 'F' & key.KeyChar != 'B' & key.KeyChar != 'L' & key.KeyChar != 'R')
                    continue;

                robot.Move(key.KeyChar.ToString());

                Repaint();

            } while (key.Key != ConsoleKey.Escape);
        }

        private static void Repaint()
        {
            Console.Clear();
            Console.WriteLine();
            for (int line = 20; line >= 0; line--)
            {
                WriteLine(line);
            }
            Console.WriteLine("──┼──────────────────────────────────────────────────────────────");
            Console.WriteLine("  │                               1  1  1  1  1  1  1  1  1  1  2");
            Console.WriteLine("  │ 0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5  6  7  8  9  0");
            Console.WriteLine();
            Console.WriteLine("Insert command (F)orward / (B)ackward / (L)eft / (R)ight:");
        }

        private static void WriteLine(int line)
        {
            if (robot.Position.Y != line)
            {
                Console.WriteLine(string.Format("{0,2}│", line));
                return;
            }

            var lineString = string.Format("{0,2}│", line);
            lineString += new string(' ', robot.Position.X * 3 + 1);

            if (robot.Position.Orientation.Equals(Orientation.North))
                lineString += '┴';
            if (robot.Position.Orientation.Equals(Orientation.East))
                lineString += '├';
            if (robot.Position.Orientation.Equals(Orientation.South))
                lineString += '┬';
            if (robot.Position.Orientation.Equals(Orientation.West))
                lineString += '┤';

            Console.WriteLine(lineString);
        }
    }
}
