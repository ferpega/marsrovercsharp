﻿using MarsRover;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarsRoverTests
{
    public class BackwardCommandTests
    {
        [TestCaseSource(typeof(CommandDataTestsClass), "BackwardDataCases")]
        public Position Execute_FromDefinedPosition_GoBack(int timesToExecute, Position initialPosition)
        {
            var sut = new BackwardCommand();
            var result = initialPosition;

            for (int i = 0; i < timesToExecute; i++)
            {
                result = sut.Execute(result);
            }

            return result;
        }
    }
}
