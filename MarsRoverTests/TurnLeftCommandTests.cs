﻿using MarsRover;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarsRoverTests
{
    public class TurnLeftCommandTests
    {
        [TestCaseSource(typeof(CommandDataTestsClass), "TurnLeftDataCases")]
        public Position Execute_FromDefinedPosition_TurnsLeft(int timesToExecute, Position initialPosition)
        {
            var sut = new TurnLeftCommand();
            var result = initialPosition;

            for (int i = 0; i < timesToExecute; i++)
            {
                result = sut.Execute(result);
            }

            return result;
        }      
    }
}
