﻿using MarsRover;
using NUnit.Framework;

namespace MarsRoverTests
{
    public class ForwardCommandTests
    {
        [TestCaseSource(typeof(CommandDataTestsClass), "ForwardDataCases")]
        public Position Execute_FromDefinedPosition_GoForward(int timesToExecute, Position initialPosition)
        {
            var sut = new ForwardCommand();
            var result = initialPosition;

            for (int i = 0; i < timesToExecute; i++)
            {
                result = sut.Execute(result);   
            }

            return result;
        }                
    }
}
