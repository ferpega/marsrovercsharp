﻿using MarsRover;
using NUnit.Framework;
using System.Collections;

namespace MarsRoverTests
{
    public class CommandDataTestsClass
    {
        public static IEnumerable ForwardDataCases
        {
            get
            {
                yield return new TestCaseData(1, new Position { Orientation = Orientation.North })
                                        .Returns(new Position { Orientation = Orientation.North, Y = 1 });
                yield return new TestCaseData(1, new Position { Orientation = Orientation.East })
                                        .Returns(new Position { Orientation = Orientation.East, X = 1 });
                yield return new TestCaseData(1, new Position { Orientation = Orientation.South })
                                        .Returns(new Position { Orientation = Orientation.South, Y = -1 });
                yield return new TestCaseData(1, new Position { Orientation = Orientation.West })
                                        .Returns(new Position { Orientation = Orientation.West, X = -1 });

                yield return new TestCaseData(2, new Position { Orientation = Orientation.North })
                                        .Returns(new Position { Orientation = Orientation.North, Y = 2 });
                yield return new TestCaseData(3, new Position { Orientation = Orientation.East })
                                        .Returns(new Position { Orientation = Orientation.East, X = 3 });
                yield return new TestCaseData(4, new Position { Orientation = Orientation.South })
                                        .Returns(new Position { Orientation = Orientation.South, Y = -4 });
                yield return new TestCaseData(5, new Position { Orientation = Orientation.West })
                                        .Returns(new Position { Orientation = Orientation.West, X = -5 });
            }
        }

        public static IEnumerable BackwardDataCases
        {
            get
            {
                yield return new TestCaseData(1, new Position { Orientation = Orientation.North })
                                        .Returns(new Position { Orientation = Orientation.North, Y = -1 });
                yield return new TestCaseData(1, new Position { Orientation = Orientation.East })
                                        .Returns(new Position { Orientation = Orientation.East, X = -1 });
                yield return new TestCaseData(1, new Position { Orientation = Orientation.South })
                                        .Returns(new Position { Orientation = Orientation.South, Y = 1 });
                yield return new TestCaseData(1, new Position { Orientation = Orientation.West })
                                        .Returns(new Position { Orientation = Orientation.West, X = 1 });

                yield return new TestCaseData(2, new Position { Orientation = Orientation.North })
                                        .Returns(new Position { Orientation = Orientation.North, Y = -2 });
                yield return new TestCaseData(3, new Position { Orientation = Orientation.East })
                                        .Returns(new Position { Orientation = Orientation.East, X = -3 });
                yield return new TestCaseData(4, new Position { Orientation = Orientation.South })
                                        .Returns(new Position { Orientation = Orientation.South, Y = 4 });
                yield return new TestCaseData(5, new Position { Orientation = Orientation.West })
                                        .Returns(new Position { Orientation = Orientation.West, X = 5 });
            }
        }

        public static IEnumerable TurnRightDataCases
        {
            get
            {
                yield return new TestCaseData(1, new Position { Orientation = Orientation.North })
                                        .Returns(new Position { Orientation = Orientation.East });
                yield return new TestCaseData(1, new Position { Orientation = Orientation.East })
                                        .Returns(new Position { Orientation = Orientation.South });
                yield return new TestCaseData(1, new Position { Orientation = Orientation.South })
                                        .Returns(new Position { Orientation = Orientation.West });
                yield return new TestCaseData(1, new Position { Orientation = Orientation.West })
                                        .Returns(new Position { Orientation = Orientation.North });

                yield return new TestCaseData(2, new Position { Orientation = Orientation.North })
                                        .Returns(new Position { Orientation = Orientation.South });
                yield return new TestCaseData(3, new Position { Orientation = Orientation.East })
                                        .Returns(new Position { Orientation = Orientation.North });
                yield return new TestCaseData(4, new Position { Orientation = Orientation.South })
                                        .Returns(new Position { Orientation = Orientation.South });
                yield return new TestCaseData(5, new Position { Orientation = Orientation.West })
                                        .Returns(new Position { Orientation = Orientation.North });
            }
        }

        public static IEnumerable TurnLeftDataCases
        {
            get
            {
                yield return new TestCaseData(1, new Position { Orientation = Orientation.North })
                                        .Returns(new Position { Orientation = Orientation.West });
                yield return new TestCaseData(1, new Position { Orientation = Orientation.East })
                                        .Returns(new Position { Orientation = Orientation.North });
                yield return new TestCaseData(1, new Position { Orientation = Orientation.South })
                                        .Returns(new Position { Orientation = Orientation.East });
                yield return new TestCaseData(1, new Position { Orientation = Orientation.West })
                                        .Returns(new Position { Orientation = Orientation.South });

                yield return new TestCaseData(2, new Position { Orientation = Orientation.North })
                                        .Returns(new Position { Orientation = Orientation.South });
                yield return new TestCaseData(3, new Position { Orientation = Orientation.East })
                                        .Returns(new Position { Orientation = Orientation.South });
                yield return new TestCaseData(4, new Position { Orientation = Orientation.South })
                                        .Returns(new Position { Orientation = Orientation.South });
                yield return new TestCaseData(5, new Position { Orientation = Orientation.West })
                                        .Returns(new Position { Orientation = Orientation.South });
            }
        }
    }
}
