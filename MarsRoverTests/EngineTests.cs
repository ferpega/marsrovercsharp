﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MarsRover;
using NUnit.Framework;

namespace MarsRoverTests
{
    public class EngineTests
    {
        private const int LIMIT_WIDTH = 20;
        private const int LIMIT_HEIGH = 20;
        private IEngineParser _engineParser = new EngineParser();

        [Test]
        public void Parse_WithUnknowCommand_ReturnsSamePosition()
        {
            var sut = new Engine(LIMIT_WIDTH, LIMIT_HEIGH, _engineParser);
            var initialPosition = new Position { Y = 5, X = 5 };

            var newPosition = sut.Execute(initialPosition, 'U');
            Assert.That(newPosition, Is.EqualTo(new Position { Y = 5, X = 5 }));
        }

        [Test]
        public void Parse_WithChar_F_GoForward()
        {
            var sut = new Engine(LIMIT_WIDTH, LIMIT_HEIGH, _engineParser);
            var initialPosition = new Position { Orientation = Orientation.North };
            
            var newPosition = sut.Execute(initialPosition, 'F');
            Assert.That(newPosition, Is.EqualTo(new Position { Y = 1 }));
        }
        [Test]
        public void Parse_WithChar_F_OnPosition_YEqualZero_BeyondLimits_GoForward()
        {
            var sut = new Engine(LIMIT_WIDTH, LIMIT_HEIGH, _engineParser);
            var initialPosition = new Position { Orientation = Orientation.South };

            var newPosition = sut.Execute(initialPosition, 'F');
            Assert.That(newPosition, Is.EqualTo(new Position { Orientation = Orientation.South, Y = LIMIT_HEIGH }));
        }
        [Test]
        public void Parse_WithChar_F_OnPosition_YEqualTop_BeyondLimits_GoForward()
        {
            var sut = new Engine(LIMIT_WIDTH, LIMIT_HEIGH, _engineParser);
            var initialPosition = new Position { Orientation = Orientation.North, Y = LIMIT_HEIGH };

            var newPosition = sut.Execute(initialPosition, 'F');
            Assert.That(newPosition, Is.EqualTo(new Position { Orientation = Orientation.North, Y = 0 }));
        }
        [Test]
        public void Parse_WithChar_F_OnPosition_XEqualZero_BeyondLimits_GoForward()
        {
            var sut = new Engine(LIMIT_WIDTH, LIMIT_HEIGH, _engineParser);
            var initialPosition = new Position { Orientation = Orientation.West };

            var newPosition = sut.Execute(initialPosition, 'F');
            Assert.That(newPosition, Is.EqualTo(new Position { Orientation = Orientation.West, X = LIMIT_HEIGH }));
        }
        [Test]
        public void Parse_WithChar_F_OnPosition_XEqualTop_BeyondLimits_GoForward()
        {
            var sut = new Engine(LIMIT_WIDTH, LIMIT_HEIGH, _engineParser);
            var initialPosition = new Position { Orientation = Orientation.East, X = LIMIT_HEIGH };

            var newPosition = sut.Execute(initialPosition, 'F');
            Assert.That(newPosition, Is.EqualTo(new Position { Orientation = Orientation.East, X = 0 }));
        }

        [Test]
        public void Parse_WithChar_B_GoBackward()
        {
            var sut = new Engine(LIMIT_WIDTH, LIMIT_HEIGH, _engineParser);
            var initialPosition = new Position { Orientation = Orientation.South };

            var newPosition = sut.Execute(initialPosition, 'B');
            Assert.That(newPosition, Is.EqualTo(new Position { Orientation = Orientation.South, Y = 1 }));
        }
        [Test]
        public void Parse_WithChar_B_OnPosition_YEqualZero_BeyondLimits_GoBackward()
        {
            var sut = new Engine(LIMIT_WIDTH, LIMIT_HEIGH, _engineParser);
            var initialPosition = new Position { Orientation = Orientation.North };

            var newPosition = sut.Execute(initialPosition, 'B');
            Assert.That(newPosition, Is.EqualTo(new Position { Orientation = Orientation.North, Y = LIMIT_HEIGH }));
        }
        [Test]
        public void Parse_WithChar_B_OnPosition_YEqualTop_BeyondLimits_GoBackward()
        {
            var sut = new Engine(LIMIT_WIDTH, LIMIT_HEIGH, _engineParser);
            var initialPosition = new Position { Orientation = Orientation.South, Y = LIMIT_HEIGH };

            var newPosition = sut.Execute(initialPosition, 'B');
            Assert.That(newPosition, Is.EqualTo(new Position { Orientation = Orientation.South, Y = 0 }));
        }
        [Test]
        public void Parse_WithChar_B_OnPosition_XEqualZero_BeyondLimits_GoBackward()
        {
            var sut = new Engine(LIMIT_WIDTH, LIMIT_HEIGH, _engineParser);
            var initialPosition = new Position { Orientation = Orientation.East };

            var newPosition = sut.Execute(initialPosition, 'B');
            Assert.That(newPosition, Is.EqualTo(new Position { Orientation = Orientation.East, X = LIMIT_HEIGH }));
        }
        [Test]
        public void Parse_WithChar_B_OnPosition_XEqualTop_BeyondLimits_GoBackward()
        {
            var sut = new Engine(LIMIT_WIDTH, LIMIT_HEIGH, _engineParser);
            var initialPosition = new Position { Orientation = Orientation.West, X = LIMIT_HEIGH };

            var newPosition = sut.Execute(initialPosition, 'B');
            Assert.That(newPosition, Is.EqualTo(new Position { Orientation = Orientation.West, X = 0 }));
        }

        [Test]
        public void Parse_WithChar_L_TurnsLeft()
        {
            var sut = new Engine(LIMIT_WIDTH, LIMIT_HEIGH, _engineParser);
            var initialPosition = new Position { Orientation = Orientation.North, X = 5, Y = 5 };

            var newPosition = sut.Execute(initialPosition, 'L');
            Assert.That(newPosition, Is.EqualTo(new Position { Orientation = Orientation.West, X = 5, Y = 5 }));
        }

        [Test]
        public void Parse_WithChar_R_TurnsRight()
        {
            var sut = new Engine(LIMIT_WIDTH, LIMIT_HEIGH, _engineParser);
            var initialPosition = new Position { Orientation = Orientation.North, X = 5, Y = 5 };

            var newPosition = sut.Execute(initialPosition, 'R');
            Assert.That(newPosition, Is.EqualTo(new Position { Orientation = Orientation.East, X = 5, Y = 5 }));
        }
    }
}