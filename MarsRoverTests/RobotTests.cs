﻿
using MarsRover;
using NUnit.Framework;
namespace MarsRoverTests
{
    [TestFixture]
    public class RobotTests
    {
        IEngineParser _engineParser;
        IEngine _engine;

        [SetUp]
        public void Setup()
        {
            _engineParser = new EngineParser();
            _engine = new Engine(20, 20, _engineParser);
        }

        [Test]
        public void MoveObject_FF_ReturnsRightPosition()
        {
            var sut = new Robot(_engine);
            var expectedPosition = new Position { Y = 2 };

            Assert.That(sut.Move("FF").Position, Is.EqualTo(expectedPosition));
        }

        [Test]
        public void MoveObject_FB_ReturnsRightPosition()
        {
            var sut = new Robot(_engine);
            var expectedPosition = new Position { Y = 1 };

            Assert.That(sut.Move("FFB").Position, Is.EqualTo(expectedPosition));
        }

        [Test]
        public void MoveObject_LongCommand_ReturnsRightPosition()
        {
            var sut = new Robot(_engine);
            var expectedPosition = new Position { Orientation = Orientation.South, X = 4, Y = 9 };

            Assert.That(sut.Move("RFFLFFFRFFFFLFFFFRBBBBLFFFFRFFRFF").Position, Is.EqualTo(expectedPosition));
        }
    }
}
