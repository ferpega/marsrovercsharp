﻿using MarsRover;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarsRoverTests
{
    public class TurnRightCommandTests
    {
        [TestCaseSource(typeof(CommandDataTestsClass), "TurnRightDataCases")]
        public Position Execute_FromDefinedPosition_TurnsRight(int timesToExecute, Position initialPosition)
        {
            var sut = new TurnRightCommand();
            var result = initialPosition;

            for (int i = 0; i < timesToExecute; i++)
            {
                result = sut.Execute(result);
            }

            return result;
        }        
    }
}
