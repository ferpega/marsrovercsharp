﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarsRover
{
    public struct Position
    {
        public Orientation Orientation { get; set; }
        public int Y { get; set; }
        public int X { get; set; }

        public override bool Equals(object obj)
        {
            if (!(obj is Position))
                return false;

            Position comparePosition = (Position)obj;

            return Orientation.Equals(comparePosition.Orientation) 
                && X.Equals(comparePosition.X) 
                && Y.Equals(comparePosition.Y);
        }

        public override string ToString()
        {
            return string.Format("Position: Orientation:{0} -> X:{1}, Y:{2}", Orientation, X, Y);
        }
    }
}
