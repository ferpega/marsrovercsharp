﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarsRover
{
    public class Engine: IEngine
    {
        private IEngineParser _engineParser;
        
        private int _widthLimit;
        private int _heightLimit;

        public Engine(int widthLimit, int heightLimit, IEngineParser engineParser)
        {
            if (engineParser == null)
                throw new ArgumentNullException("engineParser");

            _engineParser = engineParser;
            _widthLimit = widthLimit;
            _heightLimit = heightLimit;
        }

        public Position Execute(Position initialPosition, char order)
        {
            var command = _engineParser.Parse(order);

            var result = (command != null)
                ? command.Execute(initialPosition)
                : initialPosition;

            return CheckLimits(result);
        }

        private Position CheckLimits(Position position)
        {
            if (position.X > _widthLimit)
                position.X = 0;

            if (position.X < 0)
                position.X = _widthLimit;

            if (position.Y > _heightLimit)
                position.Y = 0;

            if (position.Y < 0)
                position.Y = _heightLimit;

            return position;
        }
    }
}
