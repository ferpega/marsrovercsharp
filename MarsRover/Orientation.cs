﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MarsRover
{
    public enum Orientation
    {
        North,
        East,
        South,
        West
    }
}
