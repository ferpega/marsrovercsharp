﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MarsRover
{
    public class EngineParser : IEngineParser
    {
        private const char FORWARD = 'F';
        private const char BACKWARD = 'B';
        private const char LEFT = 'L';
        private const char RIGHT = 'R';

        public ACommand Parse(char command)
        {
            ACommand result = null;

            if (command == FORWARD)
                result = new ForwardCommand();

            if (command == BACKWARD)
                result = new BackwardCommand();

            if (command == RIGHT)
                result = new TurnRightCommand();

            if (command == LEFT)
                result = new TurnLeftCommand();

            return result;
        }
    }
}
