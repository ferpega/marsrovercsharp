﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MarsRover
{
    public class ForwardCommand: ACommand
    {
        public override Position Execute(Position position)
        {
            if (position.Orientation.Equals(Orientation.North))
                position.Y++;
            if (position.Orientation.Equals(Orientation.East))
                position.X++;
            if (position.Orientation.Equals(Orientation.South))
                position.Y--;
            if (position.Orientation.Equals(Orientation.West))
                position.X--;

            return position;
        }
    }
}
