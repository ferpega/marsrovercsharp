﻿using System;
namespace MarsRover
{
    public interface IEngineParser
    {
        ACommand Parse(char command);
    }
}
