﻿
using System;
namespace MarsRover
{
    public class Robot
    {
        private IEngine _engine;

        public Robot(IEngine engine)
        {
            if (engine == null)
                throw new ArgumentNullException();

            _engine = engine;
        }

        public Position Position { get; set; }

        public Robot Move(string orders)
        {
            foreach (var order in orders.ToCharArray())
            {
                Parse(order);
            }
            return this;
        }


        private void Parse(char order)
        {
            this.Position = _engine.Execute(Position, order);
        }
    }
}
