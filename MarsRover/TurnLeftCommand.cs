﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MarsRover
{
    public class TurnLeftCommand : ACommand
    {
        public override Position Execute(Position position)
        {
            if (position.Orientation.Equals(Orientation.North))
                position.Orientation = Orientation.West;
            else if (position.Orientation.Equals(Orientation.East))
                position.Orientation = Orientation.North;
            else if (position.Orientation.Equals(Orientation.South))
                position.Orientation = Orientation.East;
            else if (position.Orientation.Equals(Orientation.West))
                position.Orientation = Orientation.South;

            return position;
        }
    }
}
