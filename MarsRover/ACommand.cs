﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MarsRover
{
    public abstract class ACommand
    {
        public abstract Position Execute(Position position);
    }
}
