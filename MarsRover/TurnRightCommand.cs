﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MarsRover
{
    public class TurnRightCommand: ACommand
    {
        public override Position Execute(Position position)
        {
            if (position.Orientation.Equals(Orientation.North))
                position.Orientation = Orientation.East;
            else if (position.Orientation.Equals(Orientation.East))
                position.Orientation = Orientation.South;
            else if (position.Orientation.Equals(Orientation.South))
                position.Orientation = Orientation.West;
            else if (position.Orientation.Equals(Orientation.West))
                position.Orientation = Orientation.North;

            return position;
        }
    }
}
